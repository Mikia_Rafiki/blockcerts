import os
import configargparse
from app import app

cwd = os.getcwd()
p = configargparse.getArgumentParser(app.config['CERT_TOOLS_CONFIG_ARG_NAME'],default_config_files=[os.path.join(cwd, app.config['CERT_TOOLS_CONFIG_FILE'])])