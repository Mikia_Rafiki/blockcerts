App = {
  

  init: function() {
    return App.bindEvents();
  },

  bindEvents: function() {

    /*----------------------CERTIFICATE ISSUE -----------------------------------------------*/
    $(document).on('click', '#issue-btn', App.switchToProgressView);
    /*----------------------END OF CERTIFICATE ISSUE----------------------------------------*/

    /*----------------------CERTIFICATE REVOKE-----------------------------------------------*/
    //$(document).on('click', '#issue-btn', App.switchToProgressView);
    /*----------------------END OF CERTIFICATE REVOKE----------------------------------------*/
  },

  /********************************************START OF FARMER FUNCTIONS********************************************** */
  switchToProgressView: function(event) {
    console.log("entering switchToProgressView function");

    document.getElementById("showIssueBtn").style.display = "none";
    document.getElementById("showIssueProgress").style.display = "block";
  },

  backToIssueBtnsView: function(event) {
    console.log("entering backToIssueBtnsView function");

    document.getElementById("showIssueBtn").style.display = "block";
    document.getElementById("showIssueProgress").style.display = "none";
  }

};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
