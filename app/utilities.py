import shutil
import os
import json
from app import app, db
from app.models import BlockchainCertificate

def move_certs(source_dir, dest_dir):
	source_certs = os.listdir(app.config[source_dir])
	for cert in source_certs:
		shutil.move(os.path.join(app.config[source_dir],cert), app.config[dest_dir])

def retrieve_grads(blockcerts_dir):
	if os.path.exists(os.path.join(app.config[blockcerts_dir], '.placeholder')):
		os.remove(os.path.join(app.config[blockcerts_dir], '.placeholder'))
	blockcerts = os.listdir(app.config[blockcerts_dir])
	for bcert in blockcerts:
		bcert = os.path.join(app.config[blockcerts_dir], bcert)
		with open(bcert) as read_cert:
			filename,ext = os.path.splitext(bcert)
			if(ext == '.json'):
				data = json.load(read_cert)
				grad_id = data['recipient']['identity']
				bcert_id_array = data['id'].split(':')
				bcert_id = bcert_id_array[2]
				blockchain_certificate = BlockchainCertificate(blockchain_cert_id=bcert_id, graduate_email=grad_id)
				db.session.add(blockchain_certificate)
				db.session.commit()

