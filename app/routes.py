import os
import shutil
import datetime
#import json
from flask import render_template, flash, redirect, url_for, request
from app import app, db, configure_cert_arguments
from app.forms import CertsIssuingForm, LoginForm, RegistrationForm
from werkzeug import secure_filename
from werkzeug.urls import url_parse
from app.bin.cert_tools.cert_tools import create_v2_certificate_template
from app.bin.cert_tools.cert_tools import instantiate_v2_certificate_batch
#from app import configure_cert_arguments
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, BlockchainCertificate
from app.utilities import move_certs, retrieve_grads

@app.route('/')
@app.route('/index')
@login_required
def index():
	return render_template('index.html')

@app.route('/issue', methods=['GET', 'POST'])
@login_required
def issue():
	form = CertsIssuingForm()
	if form.validate_on_submit():
		f = form.graduants.data
		filename = secure_filename(f.filename)

		#either complete this as a whole or not at all
		f.save(os.path.join(app.config['UPLOAD_FOLDER'], "batch_to_issue.csv"))
		flash('"{}"" successfully uploaded'.format(filename))
		#the following line creates the template to be used for certificates
		create_v2_certificate_template.main()
		print("certificate template created")
		#the following line instantiates the batch of certificates to be issued to the block chain
		instantiate_v2_certificate_batch.main()
		print("unsigned certificates instantiated")
		#the next line of code moves the instantiated unsigned certs to the cert_issuer folder, for issuing
		move_certs('UNSIGNED_CERTS_SRC', 'UNSIGNED_CERTS_DEST')
		print("unsigned certificates moved to cert issuer directory")
		#once successfully moved, issue the certificates
		from app.bin.cert_issuer.cert_issuer import issue_certificates
		print("blockchain certificates issued")
		#get student ids (emails in this case) from each blockchain cert in dir so each blockchain certificate is linked to student id in DB
		#retrieve_grads parses json certs, extracts grad ids, adds rows to BlockchainCertificates DB model
		retrieve_grads('BLOCKCHAIN_CERTS_SRC')
		print("blockcerts details added to database ")

		flash(" {} blockchain certificates successfully issued.".format(len(os.listdir(app.config['BLOCKCHAIN_CERTS_SRC']))))
		#once blockchain certficates are issued, move to static folder for viewing
		move_certs('BLOCKCHAIN_CERTS_SRC', 'BLOCKCHAIN_CERTS_DEST')
		#clear unsigned certificates directory in cert issuer by removing dir and re-creating it
		shutil.rmtree(app.config['UNSIGNED_CERTS_DEST'])
		path_to_create = app.config['UNSIGNED_CERTS_DEST']
		try:
			os.mkdir(path_to_create)
		except OSError:
			print ("Creation of the directory %s failed" % path_to_create)
		else:
			print ("Successfully created the directory %s " % path_to_create)
		
		#rename file in cert tools with the issued certificates to batch_issued plus timestamp
		new_file_name = "batch_issued" + str(datetime.datetime.now().timestamp()) + ".csv"
		os.rename(os.path.join(app.config['UPLOAD_FOLDER'], "batch_to_issue.csv"),os.path.join(app.config['UPLOAD_FOLDER'], new_file_name))
	return render_template('issue.html', form=form)

@app.route('/revoke')
@login_required
def revoke():
	#either revoke from excel sheet or revoke through form
	return render_template('revoke.html')

@app.route('/valiate')
@login_required
def validate():
	#show settings for certificate tools and issuing with edit option
	return render_template('validate.html')

@app.route('/reports')
@login_required
def reports():
	#add view of all blockcerts issued to sames graduate id
	return render_template('reports.html')

@app.route('/settings')
@login_required
def settings():
	#show settings for certificate tools and issuing with edit option
	return render_template('settings.html')

#----------------------------------------------registration and logging in/out template rendering--------------------------------------------

@app.route('/login', methods =['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		return redirect(url_for('index'))
	form = LoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email=form.email.data).first()
		if user is None or not user.check_password(form.password.data):
			flash('Invalid email or password')
			return redirect(url_for('login'))
		login_user(user, remember=form.remember_me.data)
		next_page = request.args.get('next')
		if not next_page or url_parse(next_page).netloc !='':
			next_page=url_for('index')
		return redirect(next_page)
	return render_template('login.html', title="Sign In", form=form)

@app.route('/register', methods =['GET', 'POST'])
def register():
	if current_user.is_authenticated:
		return redirect(url_for('index'))
	form = RegistrationForm()
	if form.validate_on_submit():
		user = User(username=form.username.data, email=form.email.data)
		user.set_password(form.password.data)
		db.session.add(user)
		db.session.commit()
		flash('You have successfully registered as a user.')
		return redirect(url_for('index'))
	return render_template('register.html', title="Register", form=form)

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('index'))

#for testing only!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@app.route('/view_certs')
def view_certs():
	certs = BlockchainCertificate.query.all()
	for cert in certs:
		db.session.delete(cert)
	db.session.commit()
	print(BlockchainCertificate.query.all())