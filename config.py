import os
from pathlib import Path

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
	#flask and extensions use 'SECRET_KEY' as a cryptograhic key e.g for generating signatures and tokens
	#flask-wtf uses it to protect web forms against Cros Site Request Forgery (CSRF)
	#in this case 'SECRET_KEY' is either set as the value of a same-name environment variable (prefererable for production) or a hardcoded string
	SECRET_KEY=os.environ.get('SECRET_KEY') or 'hardcoded-secret-key'
	UPLOAD_FOLDER = Path('app/bin/cert_tools/data/rosters')
	UNSIGNED_CERTS_SRC = Path('app/bin/cert_tools/data/unsigned_certificates')
	UNSIGNED_CERTS_DEST = Path('app/bin/cert_issuer/data/unsigned_certificates')
	BLOCKCHAIN_CERTS_SRC = Path('app/bin/cert_issuer/data/blockchain_certificates')
	BLOCKCHAIN_CERTS_DEST = Path('app/static/blockchain_certificates')
	CERT_TOOLS_CONFIG_FILE = Path('app/bin/cert_tools/conf.ini')
	CERT_TOOLS_CONFIG_ARG_NAME = 'cert_tools_parser'
	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///'+os.path.join(basedir,'app.db')
	SQLALCHEMY_TRACK_MODIFICATIONS = False